package org.taruts.gitLabContainerGradlePlugin

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.Setting
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.net.URL

open class ConfigureSettingsTask : DefaultTask() {

    init {
        group = "GitLab container"
    }

    @TaskAction
    fun action() {

        val gitLabContainerPluginExtension = project.extensions.findByType(
            GitLabContainerPluginExtension::class.java
        )!!

        val gitLabApi = loginToGitLabApi(
            url = gitLabContainerPluginExtension.url.get(),
            username = gitLabContainerPluginExtension.username.get(),
            password = gitLabContainerPluginExtension.password.get()
        )

        try {
            gitLabApi.applicationSettingsApi.updateApplicationSetting(
                Setting.ALLOW_LOCAL_REQUESTS_FROM_WEB_HOOKS_AND_SERVICES,
                true
            )
        } catch (e: GitLabApiException) {
            throw RuntimeException(e)
        }
    }

    private fun loginToGitLabApi(url: URL, username: String, password: String): GitLabApi {
        return GitLabApi.oauth2Login(
            url.toString(),
            username,
            password,
            true
        )
    }
}
