package org.taruts.gitLabContainerGradlePlugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.taruts.gitLabContainerGradlePlugin.utils.ContainerUtils
import org.taruts.processUtils.ProcessRunner

open class StopContainerTask : DefaultTask() {

    init {
        group = "GitLab container"
    }

    @TaskAction
    fun action() {
        // println("stopContainer. Start")
        if (ContainerUtils.containerExists()) {
            // println("containerExists() == true")

            @Suppress("UNUSED_VARIABLE")
            val output: String = ProcessRunner.runProcess(
                project.projectDir,
                "docker", "container", "stop", "gitlab.taruts.org"
            )

            // println("Docker output: $output")
        }
        // println("stopContainer. End")
    }
}
