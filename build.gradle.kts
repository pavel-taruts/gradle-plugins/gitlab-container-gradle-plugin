import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.10"
    `java-gradle-plugin`
    `maven-publish`
    id("com.gradle.plugin-publish") version "1.0.0"
}

group = "org.taruts"
version = "1.0.0"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.taruts:taruts-process-utils:1.0.3")

    // Utils
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("commons-io:commons-io:2.11.0")

    // Other libraries
    implementation("org.slf4j:slf4j-simple:1.7.36")
    implementation("org.gitlab4j:gitlab4j-api:5.0.1")
    implementation("io.projectreactor.netty:reactor-netty-http:1.0.18")
    implementation("io.netty:netty-transport-native-unix-common:4.1.74.Final")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

pluginBundle {
    website = "https://gitlab.com/pavel-taruts/gradle-plugins/gitlab-container-gradle-plugin"
    vcsUrl = "https://gitlab.com/pavel-taruts/gradle-plugins/gitlab-container-gradle-plugin.git"
    tags = listOf("git", "gitLab", "docker", "container")
}

gradlePlugin {
    plugins {
        create("gitLabContainer") {
            id = "org.taruts.gitlab-container"
            displayName = "GitLab container"

            description = """
            Adds tasks for manipulations with a GitLab instance running in a local Docker container. 
            The operations are: container creation, user creation, deleting the container, 
            deleting the container data etc.
            """.trimIndent()

            implementationClass = "org.taruts.gitLabContainerGradlePlugin.GitLabContainerPlugin"
        }
    }
}
